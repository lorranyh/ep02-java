package Model;

import static java.lang.Math.cos;
import java.util.ArrayList;

public class Uc3{
    private double amplitudeTensao;
    private double angulodeTensao;
    private double amplitudeHarm1;
    private double amplitudeHarm2;
    private double amplitudeHarm3;
    private double amplitudeHarm4;
    private double amplitudeHarm5;
    private double amplitudeHarm6;
    private double anguloHarm1;
    private double anguloHarm2;
    private double anguloHarm3;
    private double anguloHarm4;
    private double anguloHarm5;
    private double anguloHarm6;
    private double ordemHarm1;
    private double ordemHarm2;
    private double ordemHarm3;
    private double ordemHarm4;
    private double ordemHarm5;
    private double ordemHarm6;

    
    public double getAmplitudeTensao() {
        return amplitudeTensao;
    }

    public void setAmplitudeTensao(double amplitudeTensao) {
        this.amplitudeTensao = amplitudeTensao;
    }

    public double getAngulodeTensao() {
        return angulodeTensao;
    }

    public void setAngulodeTensao(double angulodeTensao) {
    	if(angulodeTensao > 180 || angulodeTensao < -180)
			throw new IllegalArgumentException("Valor do angulo da tensao inválido");

		else
			this.angulodeTensao = angulodeTensao;
    }
    public double getAmplitudeHarm1(){
        return amplitudeHarm1;
    }

    public void setAmplitudeHarm1( double amplitudeHarm1) {
        this.amplitudeHarm1 = amplitudeHarm1;
    }

    public double getAmplitudeHarm2(){
        return amplitudeHarm2;
    }

    public void setAmplitudeHarm2( double amplitudeHarm2) {
        this.amplitudeHarm2 = amplitudeHarm2;
    }

    public double getAmplitudeHarm3(){
        return amplitudeHarm3;
    }

    public void setAmplitudeHarm3( double amplitudeHarm3) {
        this.amplitudeHarm3 = amplitudeHarm3;
    }
    public double getAmplitudeHarm4(){
        return amplitudeHarm4;
    }

    public void setAmplitudeHarm4( double amplitudeHarm4) {
        this.amplitudeHarm4 = amplitudeHarm4;
    }

    public double getAmplitudeHarm5(){
        return amplitudeHarm5;
    }

    public void setAmplitudeHarm5( double amplitudeHarm5) {
        this.amplitudeHarm5 = amplitudeHarm5;
    }

    public double getAmplitudeHarm6(){
    return amplitudeHarm6;
    }

    public void setAmplitudeHarm6( double amplitudeHarm6) {
        this.amplitudeHarm6 = amplitudeHarm6;
    }

    public double getAnguloHarm1(){
        return anguloHarm1;
    }

    public void setAnguloHarm1( double anguloHarm1){
    	
        this.anguloHarm1 = anguloHarm1;
    }

    public double getAnguloHarm2(){
        return anguloHarm2;
    }

    public void setAnguloHarm2( double anguloHarm2){
        this.anguloHarm2 = anguloHarm2;
    }

    public double getAnguloHarm3(){
        return anguloHarm3;
    }

    public void setAnguloHarm3( double anguloHarm3){
        this.anguloHarm3 = anguloHarm3;
    }

    public double getAnguloHarm4(){
        return anguloHarm4;
    }

    public void setAnguloHarm4( double anguloHarm4){
        this.anguloHarm4 = anguloHarm4;
    }

    public double getAnguloHarm5(){
        return anguloHarm5;
    }

    public void setAnguloHarm5( double anguloHarm5){
        this.anguloHarm5 = anguloHarm5;
    }

    public double getAnguloHarm6(){
        return anguloHarm6;
    }

    public void setAnguloHarm6( double anguloHarm6){
        this.anguloHarm6 = anguloHarm6;
    }

     public double getOrdemHarm1() {
        return ordemHarm1;
    }

    public void setOrdemHarm1(double ordemHarm1) {
        this.ordemHarm1 = ordemHarm1;
    }

    public double getOrdemHarm2() {
        return ordemHarm2;
    }

    public void setOrdemHarm2(double ordemHarm2) {
        this.ordemHarm2 = ordemHarm2;
    }

    public double getOrdemHarm3() {
        return ordemHarm3;
    }

    public void setOrdemHarm3(double ordemHarm3) {
        this.ordemHarm3 = ordemHarm3;
    }

    public double getOrdemHarm4() {
        return ordemHarm4;
    }

    public void setOrdemHarm4(double ordemHarm4) {
        this.ordemHarm4 = ordemHarm4;
    }

    public double getOrdemHarm5() {
        return ordemHarm5;
    }

    public void setOrdemHarm5(double ordemHarm5) {
        this.ordemHarm5 = ordemHarm5;
    }

    public double getOrdemHarm6() {
        return ordemHarm6;
    }

    public void setOrdemHarm6(double ordemHarm6) {
        this.ordemHarm6 = ordemHarm6;
    }

  
    public ArrayList<Double> formadeOndaTensao(){
        
           ArrayList<Double> lista_OndaTensao = new ArrayList<>();
           double t= 0.0;
            while(t<=0.1){
                lista_OndaTensao.add(getAmplitudeTensao()*(cos((2*3.14*60*t+ Math.toRadians(getAngulodeTensao())))));
                t += 0.0005;
            }
            
            return (lista_OndaTensao);
        }
    
    public ArrayList<Double> ondasHarmonicas1(){
        ArrayList<Double> harm1 = new ArrayList<>();
        double i= 0.0;
        while(i<=0.1) {
            harm1.add(getAmplitudeHarm1()*cos((getOrdemHarm1()*2*3.14*60*i) + getAnguloHarm1()));
            i+= 0.0005;
        }
        return (harm1);
    }
    public ArrayList<Double> ondasHarmonicas2(){
        ArrayList<Double> harm2 = new ArrayList<>();
        double i= 0.0;
        
        while(i<=0.1) {
            harm2.add(getAmplitudeHarm2()*cos((getOrdemHarm2()*2*3.14*60*i) + getAnguloHarm2()));
            i+= 0.0005;
        }
        return (harm2);
    }
    
    public ArrayList<Double> ondasHarmonicas3(){
        ArrayList<Double> harm3 = new ArrayList<>();
        double i= 0.0;
        
        while(i<=0.1) {
            harm3.add(getAmplitudeHarm3()*cos((getOrdemHarm3()*2*3.14*60*i) + getAnguloHarm3()));
            i+= 0.0005;
        }
        return (harm3);
    }
    
    public ArrayList<Double> ondasHarmonicas4(){
        ArrayList<Double> harm4 = new ArrayList<>();

        double i= 0.0;
        
        while(i<=0.1)  {
            harm4.add(getAmplitudeHarm4()*(((getOrdemHarm4()*2*3.14*60*i) + getAnguloHarm4())));
            i+= 0.0005;
        }
        return (harm4);
    }
    
    public ArrayList<Double> ondasHarmonicas5(){
        ArrayList<Double> harm5 = new ArrayList<>();

        double i= 0.0;
        
        while(i<=0.1)  {
            harm5.add(getAmplitudeHarm5()*cos((getOrdemHarm5()*2*3.14*60*i) + getAnguloHarm5()));
            i+= 0.0005;
        }
        return (harm5);
    }
    public ArrayList<Double> ondasHarmonicas6(){
        ArrayList<Double> harm6 = new ArrayList<>();

        double i= 0.0;
        
        while(i<=0.1)  {
            harm6.add(getAmplitudeHarm6()*cos((getOrdemHarm6()*2*3.14*60*i) + getAnguloHarm6()));
            i+= 0.0005;
        }
        return (harm6);
    }
    
    public ArrayList<Double> ondaDistorcida (){
        ArrayList<Double> soma = new ArrayList<>();

        double i= 0.0;
        
        while(i<=0.1)  {
            soma.add(getAmplitudeTensao()*cos((2*3.14*60*i) + getAngulodeTensao())+
                    getAmplitudeHarm1()*cos((getOrdemHarm1()*2*3.14*60*i) + getAnguloHarm1()) +
                    getAmplitudeHarm2()*cos((getOrdemHarm2()*2*3.14*60*i) + getAnguloHarm2()) +
                    getAmplitudeHarm3()*cos((getOrdemHarm3()*2*3.14*60*i) + getAnguloHarm3()) +
                    getAmplitudeHarm4()*cos((getOrdemHarm4()*2*3.14*60*i) + getAnguloHarm4()) +
                    getAmplitudeHarm5()*cos((getOrdemHarm5()*2*3.14*60*i) + getAnguloHarm5()) +
                    getAmplitudeHarm6()*cos((getOrdemHarm6()*2*3.14*60*i) + getAnguloHarm6())          
            );
            i+= 0.0005;
            
            
        }
        return (soma);
    }
    
 
   
}