package View;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.FluxoPotenciaFundamental;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class CasoDeUso2 extends JFrame {
	

	private JPanel contentPane;
	
	FluxoPotenciaFundamental dadosUC2;
	
	 	private GraphPanel graficoTensao;
	    private GraphPanel graficoCorrente;
	    private GraphPanel graficoPotInstantanea;
	    private JTextField AmpTensao;
	    private JTextField AngTensao;
	    private JTextField AmpCorrente;
	    
	    private JTextField valorPotAtiva;
	    private JTextField valorPotRelativa;
	    private JTextField valorPotAparente;
	    private JTextField valorFatorPotencia;
	    
	    

	/**
	 * Create the frame.
	 */
	public CasoDeUso2() {
		dadosUC2 = new FluxoPotenciaFundamental();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1200, 1200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_Caso2 = new JPanel();
		contentPane.add(panel_Caso2, BorderLayout.CENTER);
		panel_Caso2.setLayout(new GridLayout(3, 2, 0, 0));
		
		JPanel panel_GeralTensao = new JPanel();
		panel_Caso2.add(panel_GeralTensao);
		panel_GeralTensao.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_graficoTensao = new JPanel();
		panel_GeralTensao.add(panel_graficoTensao);
		panel_graficoTensao.setLayout(new GridLayout(1, 0, 0, 0));
		graficoTensao=new GraphPanel(new ArrayList<>());
		panel_graficoTensao.add(graficoTensao);
		
		JPanel panel_EntradaTensao = new JPanel();
		panel_GeralTensao.add(panel_EntradaTensao);
		panel_EntradaTensao.setLayout(null);
		
		AmpTensao = new JTextField();
		AmpTensao.setBounds(57, 75, 78, 31);
		panel_EntradaTensao.add(AmpTensao);
		AmpTensao.setColumns(10);
		
		JLabel lblAmplitude = new JLabel("Amplitude:");
		lblAmplitude.setBounds(57, 48, 121, 15);
		panel_EntradaTensao.add(lblAmplitude);
		
		JLabel lblnguloDeFase = new JLabel("Ângulo de Fase:");
		lblnguloDeFase.setBounds(190, 48, 121, 15);
		panel_EntradaTensao.add(lblnguloDeFase);
		
		AngTensao = new JTextField();
		AngTensao.setColumns(10);
		AngTensao.setBounds(190, 75, 78, 31);
		panel_EntradaTensao.add(AngTensao);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double x_AmpTensao = Double.parseDouble(AmpTensao.getText());
					double x_AngTensao = Double.parseDouble(AngTensao.getText());
					dadosUC2.setAmplitudeTensao(x_AmpTensao);
					dadosUC2.setAngulodaTensao(x_AngTensao);
					graficoTensao.setScores(dadosUC2.formadeOndaTensao());
					graficoPotInstantanea.setScores(dadosUC2.formaPotInstantanea());
					valorPotAtiva.setText(Double.toString(dadosUC2.CalculodaPotenciaAtiva()));
					valorPotRelativa.setText(Double.toString(dadosUC2.CalculodaPotenciaReativa()));
					valorPotAparente.setText(Double.toString(dadosUC2.CalculodaPotenciaAparente()));
					valorFatorPotencia.setText(Double.toString(dadosUC2.calculodoFatorPotencia()));
					}
					
					catch(NumberFormatException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(IllegalArgumentException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
			
			
		});
		btnEnviar.setBounds(114, 125, 107, 25);
		panel_EntradaTensao.add(btnEnviar);
	
		JPanel panel_GeralCorrente = new JPanel();
		panel_Caso2.add(panel_GeralCorrente);
		panel_GeralCorrente.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_graficoCorrente = new JPanel();
		panel_GeralCorrente.add(panel_graficoCorrente);
		panel_graficoCorrente.setLayout(new GridLayout(1, 0, 0, 0));
		graficoCorrente=new GraphPanel(new ArrayList<>());
		panel_graficoCorrente.add(graficoCorrente);
		
		JPanel panel_EntradaCorrente = new JPanel();
		panel_GeralCorrente.add(panel_EntradaCorrente);
		panel_EntradaCorrente.setLayout(null);
	
		
		JLabel lblAmplitudeCorrente = new JLabel("Amplitude:");
		lblAmplitudeCorrente.setBounds(57, 48, 121, 15);
		panel_EntradaCorrente.add(lblAmplitudeCorrente);
		
		JLabel lblAnguloDeCorrente = new JLabel("Ângulo de Fase:");
		lblAnguloDeCorrente.setBounds(190, 48, 121, 15);
		panel_EntradaCorrente.add(lblAnguloDeCorrente);
		
		JTextField AngCorrente = new JTextField();
		AngCorrente.setColumns(10);
		AngCorrente.setBounds(190, 75, 78, 31);
		panel_EntradaCorrente.add(AngCorrente);
		btnEnviar.setBounds(114, 125, 107, 25);
		
		JTextField AmplitudeCorrent = new JTextField();
		AmplitudeCorrent.setColumns(10);
		AmplitudeCorrent.setBounds(57, 75, 78, 31);
		panel_EntradaCorrente.add(AmplitudeCorrent);
		
		JButton btnEnviar2 = new JButton("Enviar");
		btnEnviar2.setBounds(114, 125, 107, 25);
		panel_EntradaCorrente.add(btnEnviar2);
		
		JPanel panel_Saida = new JPanel();
		panel_Caso2.add(panel_Saida);
		panel_Saida.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_graficoPotInstantanea = new JPanel();
		panel_Saida.add(panel_graficoPotInstantanea);
		panel_graficoPotInstantanea.setLayout(new GridLayout(1, 0, 0, 0));
		graficoPotInstantanea=new GraphPanel(new ArrayList<>());
		panel_graficoPotInstantanea.add(graficoPotInstantanea);
		
		JPanel panel_SaidaPotInstantanea = new JPanel();
		panel_Saida.add(panel_SaidaPotInstantanea);
		panel_SaidaPotInstantanea.setLayout(null);
		
		JTextField valorPotAtiva = new JTextField();
		valorPotAtiva.setBounds(156, 44, 162, 19);
		panel_SaidaPotInstantanea.add(valorPotAtiva);
		valorPotAtiva.setColumns(10);
		
		JLabel lblvalorPotAtiva = new JLabel("Potência Ativa");
		lblvalorPotAtiva.setBounds(12, 48, 126, 15);
		panel_SaidaPotInstantanea.add(lblvalorPotAtiva);
		
		JTextField valorPotRelativa = new JTextField();
		valorPotRelativa .setColumns(10);
		valorPotRelativa .setBounds(156, 86, 162, 19);
		panel_SaidaPotInstantanea.add(valorPotRelativa );
		
		JLabel lblvalorPotRelativa  = new JLabel("Potência Relativa");
		lblvalorPotRelativa .setBounds(12, 90, 126, 15);
		panel_SaidaPotInstantanea.add(lblvalorPotRelativa );
		
		JTextField valorPotAparente = new JTextField();
		valorPotAparente.setColumns(10);
		valorPotAparente.setBounds(156, 130, 162, 19);
		panel_SaidaPotInstantanea.add(valorPotAparente);
		
		JLabel lblvalorPotAparente = new JLabel("Potência Aparente");
		lblvalorPotAparente.setBounds(12, 132, 142, 15);
		panel_SaidaPotInstantanea.add(lblvalorPotAparente);
		
		JTextField valorFatorPotencia = new JTextField();
		valorFatorPotencia.setColumns(10);
		valorFatorPotencia.setBounds(156, 173, 162, 19);
		panel_SaidaPotInstantanea.add(valorFatorPotencia);
		
		JLabel lblvalorFatorPotencia = new JLabel("Fator de Potência");
		lblvalorFatorPotencia.setBounds(12, 175, 126, 15);
		panel_SaidaPotInstantanea.add(lblvalorFatorPotencia);
		
	

	}
}
