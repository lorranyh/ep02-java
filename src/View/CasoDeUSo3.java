package View;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Model.Uc3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CasoDeUSo3 extends JFrame {
	
	Uc3 dadosUc3;
	
	private GraphPanel graficoPotFundamental;
	private GraphPanel graficoHarm1;
	private GraphPanel graficoHarm2;
	private GraphPanel graficoHarm3;
	private GraphPanel graficoHarm4;
	private GraphPanel graficoHarm5;
	private GraphPanel graficoHarm6;
	private GraphPanel graficoSaida;

	private JPanel contentPane;
	private JTextField Amp_fundamental;
	private JTextField Ang_fundamental;
	private JTextField Amp_harm1, Amp_harm2, Amp_harm3, Amp_harm4, Amp_harm5, Amp_harm6;
	private JTextField Ang_harm1, ang_harm2, ang_harm3, ord_harm4, Ord_harm5, ord_harm6;
	private JTextField ord_harm1, ord_harm2, ord_harm3, ang_harm4, ang_harm5, ang_harm6;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public CasoDeUSo3() {
		dadosUc3 = new Uc3();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 2000, 2000);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panelUC3 = new JPanel();
		panelUC3.setBounds(5, 5, 1845, 1046);
		contentPane.add(panelUC3);
		panelUC3.setLayout(null);
		
		JPanel panelEntradaOndaFundamental = new JPanel();
		panelEntradaOndaFundamental.setBounds(0, 0, 1845, 254);
		panelUC3.add(panelEntradaOndaFundamental);
		panelEntradaOndaFundamental.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panelGraficoFundamental = new JPanel();
		panelEntradaOndaFundamental.add(panelGraficoFundamental);
		panelGraficoFundamental.setLayout(new GridLayout(1, 1, 0, 0));
		graficoPotFundamental=new GraphPanel(new ArrayList<>());
		panelGraficoFundamental.add(graficoPotFundamental);
		graficoPotFundamental.setLayout(null);
		
		JLabel lblComponenteFunadamental = new JLabel("Componente Funadamental");
		lblComponenteFunadamental.setBounds(215, 220, 302, 15);
		graficoPotFundamental.add(lblComponenteFunadamental);
		
		JPanel panelValoresUC3 = new JPanel();
		panelEntradaOndaFundamental.add(panelValoresUC3);
		panelValoresUC3.setLayout(null);
		
		Amp_fundamental = new JTextField();
		Amp_fundamental.setBounds(23, 47, 75, 26);
		panelValoresUC3.add(Amp_fundamental);
		Amp_fundamental.setColumns(10);
		
		JLabel lblAmplitude = new JLabel("Amplitude");
		lblAmplitude.setBounds(135, 49, 99, 15);
		panelValoresUC3.add(lblAmplitude);
		
		Ang_fundamental = new JTextField();
		Ang_fundamental.setColumns(10);
		Ang_fundamental.setBounds(23, 86, 75, 26);
		panelValoresUC3.add(Ang_fundamental);
		
		JLabel lblAnguloFase = new JLabel("Angulo Fase");
		lblAnguloFase.setBounds(135, 88, 99, 15);
		panelValoresUC3.add(lblAnguloFase);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				double variavelAmplitudeFundamental = Double.parseDouble(Amp_fundamental.getText());
				double variavelAnguloFundamental = Double.parseDouble(Ang_fundamental.getText());
				dadosUc3.setAmplitudeTensao(variavelAmplitudeFundamental);
				dadosUc3.setAmplitudeTensao(variavelAnguloFundamental);
				graficoPotFundamental.setScores(dadosUc3.formadeOndaTensao());
				}
				catch(NumberFormatException err) {
					JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				catch(IllegalArgumentException err) {
					JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}
				
				
			
		});
		btnEnviar.setBounds(117, 131, 117, 25);
		panelValoresUC3.add(btnEnviar);
		
		JRadioButton rdbtnmpares = new JRadioButton("Ímpares");
		rdbtnmpares.setBounds(583, 87, 87, 23);
		panelValoresUC3.add(rdbtnmpares);
		
		JRadioButton rdbtnPares = new JRadioButton("Pares");
		rdbtnPares.setBounds(583, 114, 75, 23);
		panelValoresUC3.add(rdbtnPares);
		
		JLabel lblNewLabel_1 = new JLabel(" Harmônicos:");
		lblNewLabel_1.setBounds(571, 63, 99, 15);
		panelValoresUC3.add(lblNewLabel_1);
	
		JComboBox N_Harmonicos = new JComboBox<Double>();
		for (int i = 1; i < 7; i++){
            N_Harmonicos.addItem(i);
        }
		
		N_Harmonicos.setBounds(615, 197, 43, 24);
		panelValoresUC3.add(N_Harmonicos);
		
		JLabel lbln_harmnica = new JLabel("Número de Harmônicos");
		lbln_harmnica.setBounds(569, 170, 173, 15);
		panelValoresUC3.add(lbln_harmnica);
		
		JPanel panel_Part2 = new JPanel();
		panel_Part2.setBounds(0, 264, 1845, 254);
		panelUC3.add(panel_Part2);
		panel_Part2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelHarmonicoTotal = new JPanel();
		panel_Part2.add(panelHarmonicoTotal);
		panelHarmonicoTotal.setLayout(new GridLayout(2, 3, 0, 0));
		
		JPanel panel_HarmonicoCima = new JPanel();
		panelHarmonicoTotal.add(panel_HarmonicoCima);
		panel_HarmonicoCima.setLayout(new GridLayout(0, 3, 0, 0));
		
		
		JPanel panel_NHarmonicos1 = new JPanel();
		panel_HarmonicoCima.add(panel_NHarmonicos1);
		panel_NHarmonicos1.setLayout(null);
		
		JPanel panel_graficoH1 = new JPanel();
		panel_graficoH1.setBounds(0, 0, 615, 127);
		panel_NHarmonicos1.add(panel_graficoH1);
		panel_graficoH1.setLayout(null);
		graficoHarm1=new GraphPanel(new ArrayList<>());
		graficoHarm1.setBounds(0, -49, 612, 217);
		panel_graficoH1.add(graficoHarm1);
		;
		
		JLabel lblNmeroDeHarmnicos = new JLabel("Número de Harmônicos: 1");
		lblNmeroDeHarmnicos.setBounds(157, 96, 275, 15);
		graficoHarm1.add(lblNmeroDeHarmnicos);
		
		
		
		JPanel panel_NHarmonicos2 = new JPanel();
		panel_HarmonicoCima.add(panel_NHarmonicos2);
		panel_NHarmonicos2.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_graficoH2 = new JPanel();
		panel_NHarmonicos2.add(panel_graficoH2);
		panel_graficoH2.setLayout(null);
		graficoHarm2=new GraphPanel(new ArrayList<>());
		graficoHarm2.setBounds(0, -24, 612, 194);
		panel_graficoH2.add(graficoHarm2);
		
		
		JLabel lblNmeroDeHarmnicos2 = new JLabel("Número de Harmônicos: 2");
		lblNmeroDeHarmnicos2.setBounds(157, 96, 275, 15);
		graficoHarm2.add(lblNmeroDeHarmnicos2);
		
		JPanel panel_NHarmonicos3 = new JPanel();
		panel_HarmonicoCima.add(panel_NHarmonicos3);
		panel_NHarmonicos3.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_graficoH3 = new JPanel();
		panel_NHarmonicos3.add(panel_graficoH3);
		panel_graficoH3.setLayout(null);
		graficoHarm3=new GraphPanel(new ArrayList<>());
		graficoHarm3.setBounds(0, -52, 615, 222);
		panel_graficoH3.add(graficoHarm3);
		
		JLabel lblNmeroDeHarmnicos3 = new JLabel("Número de Harmônicos: 3");
		lblNmeroDeHarmnicos3.setBounds(157, 96, 275, 15);
		graficoHarm3.add(lblNmeroDeHarmnicos3);
		
		JPanel panelHarmonicoBaixo = new JPanel();
		panelHarmonicoTotal.add(panelHarmonicoBaixo);
		panelHarmonicoBaixo.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel_NHarmonicosDados1 = new JPanel();
		panelHarmonicoBaixo.add(panel_NHarmonicosDados1);
		panel_NHarmonicosDados1.setLayout(null);
		
		Amp_harm1 = new JTextField();
		Amp_harm1.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados1.add(Amp_harm1);
		Amp_harm1.setColumns(10);
		
		JLabel lblAmplitude_1 = new JLabel("Amplitude");
		lblAmplitude_1.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados1.add(lblAmplitude_1);
		
		Ang_harm1 = new JTextField();
		Ang_harm1.setColumns(10);
		Ang_harm1.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados1.add(Ang_harm1);
		
		JLabel lblOrdemHarmonica1 = new JLabel("Ordem Harmônica");
		lblOrdemHarmonica1.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados1.add(lblOrdemHarmonica1);
		
		ord_harm1 = new JTextField();
		ord_harm1.setColumns(10);
		ord_harm1.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados1.add(ord_harm1);
		
		JLabel lblAnguloDeFase1 = new JLabel("Ângulo de Fase");
		lblAnguloDeFase1.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados1.add(lblAnguloDeFase1);
		
		JButton button_Harm1 = new JButton("Enviar");
		button_Harm1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double ampHarmonico1 = Double.parseDouble(Amp_harm1.getText());
					double angHarmonico1 = Double.parseDouble(Ang_harm1.getText());
					double ordHarmonico1 = Double.parseDouble(ord_harm1.getText());
					
					dadosUc3.setAmplitudeHarm1(ampHarmonico1);
					dadosUc3.setAnguloHarm1(angHarmonico1);
					dadosUc3.setOrdemHarm1(ordHarmonico1);
					
					graficoHarm1.setScores(dadosUc3.ondasHarmonicas1());
					graficoSaida.setScores(dadosUc3.ondaDistorcida());
					
					
					
					//dadosUc3.setAmplitudeTensao(variavelAmplitudeFundamental);
					//dadosUc3.setAmplitudeTensao(variavelAnguloFundamental);
					//graficoFundamental.setScores(dadosUc3.formadeOndaTensao());
					}
					catch(NumberFormatException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(IllegalArgumentException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
				
			
		});
		button_Harm1.setBounds(282, 82, 83, 25);
		panel_NHarmonicosDados1.add(button_Harm1);
		
		JPanel panel_NHarmonicosDados2 = new JPanel();
		panelHarmonicoBaixo.add(panel_NHarmonicosDados2);
		panel_NHarmonicosDados2.setLayout(null);
		
		Amp_harm2 = new JTextField();
		Amp_harm2.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados2.add(Amp_harm2);
		Amp_harm2.setColumns(10);
		
		JLabel lblAmplitude_2 = new JLabel("Amplitude");
		lblAmplitude_2.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados2.add(lblAmplitude_2);
		
		ang_harm2 = new JTextField();
		ang_harm2.setColumns(10);
		ang_harm2.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados2.add(ang_harm2);
		
		JLabel lblOrdemHarmonica2 = new JLabel("Ordem Harmônica");
		lblOrdemHarmonica2.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados2.add(lblOrdemHarmonica2);
		
		ord_harm2 = new JTextField();
		ord_harm2.setColumns(10);
		ord_harm2.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados2.add(ord_harm2);
		
		JLabel lblAnguloDeFase2 = new JLabel("Ângulo de Fase");
		lblAnguloDeFase2.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados2.add(lblAnguloDeFase2);
		
		JButton button_Harm2 = new JButton("Enviar");
		button_Harm2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double ampHarmonico2 = Double.parseDouble(Amp_harm2.getText());
					double angHarmonico2 = Double.parseDouble(ang_harm2.getText());
					double ordHarmonico2 = Double.parseDouble(ord_harm2.getText());
					
					dadosUc3.setAmplitudeHarm2(ampHarmonico2);
					dadosUc3.setAnguloHarm2(angHarmonico2);
					dadosUc3.setOrdemHarm2(ordHarmonico2);
					
					graficoHarm2.setScores(dadosUc3.ondasHarmonicas2());
					graficoSaida.setScores(dadosUc3.ondaDistorcida());
					
					
				
					}
					catch(NumberFormatException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(IllegalArgumentException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
				
			
		});
		button_Harm2.setBounds(282, 82, 83, 25);
		panel_NHarmonicosDados2.add(button_Harm2);
		
		JPanel panel_NHarmonicosDados3 = new JPanel();
		panelHarmonicoBaixo.add(panel_NHarmonicosDados3);
		panel_NHarmonicosDados3.setLayout(null);
		
		Amp_harm3 = new JTextField();
		Amp_harm3.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados3.add(Amp_harm3);
		Amp_harm3.setColumns(10);
		
		JLabel lblAmplitude_3 = new JLabel("Amplitude");
		lblAmplitude_3.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados3.add(lblAmplitude_3);
		
		ang_harm3 = new JTextField();
		ang_harm3.setColumns(10);
		ang_harm3.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados3.add(ang_harm3);
		
		JLabel lblOrdemHarmônica3 = new JLabel("Ordem Harmônica");
		lblOrdemHarmônica3.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados3.add(lblOrdemHarmônica3);
		
		ord_harm3 = new JTextField();
		ord_harm3.setColumns(10);
		ord_harm3.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados3.add(ord_harm3);
		
		JLabel lblAnguloDeFase = new JLabel("Ângulo de Fase");
		lblAnguloDeFase.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados3.add(lblAnguloDeFase);
		
		
		
		JPanel panel_part3 = new JPanel();
		panel_part3.setBounds(0, 528, 1845, 254);
		panelUC3.add(panel_part3);
		panel_part3.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panel_HarmonicoCima_Continuacao = new JPanel();
		panel_part3.add(panel_HarmonicoCima_Continuacao);
		panel_HarmonicoCima_Continuacao.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel_NHarmonicos4 = new JPanel();
		panel_HarmonicoCima_Continuacao.add(panel_NHarmonicos4);
		panel_NHarmonicos4.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_graficoH4 = new JPanel();
		panel_NHarmonicos4.add(panel_graficoH4);
		panel_graficoH4.setLayout(null);
		graficoHarm4=new GraphPanel(new ArrayList<>());
		graficoHarm4.setBounds(0, -23, 615, 240);
		panel_graficoH4.add(graficoHarm4);
		
		
		
		JLabel lblNmeroDeHarmnicos4 = new JLabel("Número de Harmônicos: 4");
		lblNmeroDeHarmnicos4.setBounds(217, 5, 181, 15);
		graficoHarm4.add(lblNmeroDeHarmnicos4);
		
		JPanel panel_NHarmonicos5 = new JPanel();
		panel_HarmonicoCima_Continuacao.add(panel_NHarmonicos5);
		panel_NHarmonicos5.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_graficoH5 = new JPanel();
		panel_NHarmonicos5.add(panel_graficoH5);
		panel_graficoH5.setLayout(null);
		graficoHarm5=new GraphPanel(new ArrayList<>());
		graficoHarm5.setBounds(0, -22, 615, 209);
		panel_graficoH5.add(graficoHarm5);
		
		
		JLabel lblNmeroDeHarmnicos5 = new JLabel("Número de Harmônicos: 5");
		lblNmeroDeHarmnicos5.setBounds(157, 96, 275, 15);
		graficoHarm5.add(lblNmeroDeHarmnicos5);
		
		JPanel panel_NHarmonicos6 = new JPanel();
		panel_HarmonicoCima_Continuacao.add(panel_NHarmonicos6);
		panel_NHarmonicos6.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_graficoH6 = new JPanel();
		panel_NHarmonicos6.add(panel_graficoH6);
		panel_graficoH6.setLayout(null);
		graficoHarm6=new GraphPanel(new ArrayList<>());
		graficoHarm6.setBounds(0, -25, 615, 212);
		panel_graficoH6.add(graficoHarm6);
		
		
		JLabel lblNmeroDeHarmnicos6 = new JLabel("Número de Harmônicos: 6");
		lblNmeroDeHarmnicos6.setBounds(157, 96, 275, 15);
		graficoHarm6.add(lblNmeroDeHarmnicos6);
		JPanel panel_HarmonicoBaixo_Continuacao = new JPanel();
		panel_part3.add(panel_HarmonicoBaixo_Continuacao);
		panel_HarmonicoBaixo_Continuacao.setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel_NHarmonicosDados4 = new JPanel();
		panel_HarmonicoBaixo_Continuacao.add(panel_NHarmonicosDados4);
		panel_NHarmonicosDados4.setLayout(null);
		
		Amp_harm4 = new JTextField();
		Amp_harm4.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados4.add(Amp_harm4);
		Amp_harm4.setColumns(10);
		
		JLabel lblAmplitude_4 = new JLabel("Amplitude");
		lblAmplitude_4.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados4.add(lblAmplitude_4);
		
		ord_harm4 = new JTextField();
		ord_harm4.setColumns(10);
		ord_harm4.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados4.add(ord_harm4);
		
		JLabel lblOdemHarmonica4 = new JLabel("Ordem Harmônica");
		lblOdemHarmonica4.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados4.add(lblOdemHarmonica4);
		
		ang_harm4 = new JTextField();
		ang_harm4.setColumns(10);
		ang_harm4.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados4.add(ang_harm4);
		
		JLabel lblAnguloDeFase4 = new JLabel("Ângulo de Fase");
		lblAnguloDeFase4.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados4.add(lblAnguloDeFase4);
		
		JButton button_Harm4 = new JButton("Enviar");
		button_Harm4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double ampHarmonico4 = Double.parseDouble(Amp_harm4.getText());
					double angHarmonico4 = Double.parseDouble(ang_harm4.getText());
					double ordHarmonico4 = Double.parseDouble(ord_harm4.getText());
					
					dadosUc3.setAmplitudeHarm4(ampHarmonico4);
					dadosUc3.setAnguloHarm4(angHarmonico4);
					dadosUc3.setOrdemHarm4(ordHarmonico4);
					
					graficoHarm4.setScores(dadosUc3.ondasHarmonicas4());
					graficoSaida.setScores(dadosUc3.ondaDistorcida());
					
					
					
				
					}
					catch(NumberFormatException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(IllegalArgumentException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
			
		});
		button_Harm4.setBounds(282, 82, 83, 25);
		panel_NHarmonicosDados4.add(button_Harm4);
		
		JPanel panel_NHarmonicosDados5 = new JPanel();
		panel_HarmonicoBaixo_Continuacao.add(panel_NHarmonicosDados5);
		panel_NHarmonicosDados5.setLayout(null);
		
		Amp_harm5 = new JTextField();
		Amp_harm5.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados5.add(Amp_harm5);
		Amp_harm5.setColumns(10);
		
		JLabel lblAmplitude_5 = new JLabel("Amplitude");
		lblAmplitude_5.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados5.add(lblAmplitude_5);
		
		Ord_harm5 = new JTextField();
		Ord_harm5.setColumns(10);
		Ord_harm5.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados5.add(Ord_harm5);
		
		JLabel lblOrdemHarmonica5 = new JLabel("Ordem Harmônica");
		lblOrdemHarmonica5.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados5.add(lblOrdemHarmonica5);
		
		ang_harm5 = new JTextField();
		ang_harm5.setColumns(10);
		ang_harm5.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados5.add(ang_harm5);
		
		JLabel lblAnguloDeFase5 = new JLabel("Ângulo de Fase");
		lblAnguloDeFase5.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados5.add(lblAnguloDeFase5);
		
		JButton button_Harm5 = new JButton("Enviar");
		button_Harm5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double ampHarmonico5 = Double.parseDouble(Amp_harm5.getText());
					double angHarmonico5 = Double.parseDouble(ang_harm5.getText());
					double ordHarmonico5 = Double.parseDouble(Ord_harm5.getText());
					
					dadosUc3.setAmplitudeHarm5(ampHarmonico5);
					dadosUc3.setAnguloHarm5(angHarmonico5);
					dadosUc3.setOrdemHarm5(ordHarmonico5);
					
					graficoHarm5.setScores(dadosUc3.ondasHarmonicas5());
					graficoSaida.setScores(dadosUc3.ondaDistorcida());
					
					
					
				
					}
					catch(NumberFormatException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					catch(IllegalArgumentException err) {
						JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
			
		});
		button_Harm5.setBounds(367, 87, 117, 25);
		panel_NHarmonicosDados5.add(button_Harm5);
		
		JPanel panel_NHarmonicosDados6 = new JPanel();
		panel_HarmonicoBaixo_Continuacao.add(panel_NHarmonicosDados6);
		panel_NHarmonicosDados6.setLayout(null);
		
		Amp_harm6 = new JTextField();
		Amp_harm6.setBounds(12, 22, 92, 33);
		panel_NHarmonicosDados6.add(Amp_harm6);
		Amp_harm6.setColumns(10);
		
		JButton button_Harm3 = new JButton("Enviar");
		button_Harm3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				double ampHarmonico3 = Double.parseDouble(Amp_harm3.getText());
				double angHarmonico3 = Double.parseDouble(ang_harm3.getText());
				double ordHarmonico3 = Double.parseDouble(ord_harm3.getText());
				
				dadosUc3.setAmplitudeHarm3(ampHarmonico3);
				dadosUc3.setAnguloHarm3(angHarmonico3);
				dadosUc3.setOrdemHarm3(ordHarmonico3);
				
				graficoHarm3.setScores(dadosUc3.ondasHarmonicas3());
				graficoSaida.setScores(dadosUc3.ondaDistorcida());
				
	
				}
			catch(NumberFormatException err) {
				JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			catch(IllegalArgumentException err) {
				JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
				
			}
			
			
		});
		
		button_Harm3.setBounds(282, 82, 83, 25);
		panel_NHarmonicosDados3.add(button_Harm3);
		
		JLabel lblAmplitude_6 = new JLabel("Amplitude");
		lblAmplitude_6.setBounds(113, 25, 103, 26);
		panel_NHarmonicosDados6.add(lblAmplitude_6);
		
		ord_harm6 = new JTextField();
		ord_harm6.setColumns(10);
		ord_harm6.setBounds(214, 22, 92, 33);
		panel_NHarmonicosDados6.add(ord_harm6);
		
		JLabel lblOrdemDeFase6 = new JLabel("Ordem Harmônica");
		lblOrdemDeFase6.setBounds(324, 31, 138, 15);
		panel_NHarmonicosDados6.add(lblOrdemDeFase6);
		
		ang_harm6 = new JTextField();
		ang_harm6.setColumns(10);
		ang_harm6.setBounds(12, 78, 92, 33);
		panel_NHarmonicosDados6.add(ang_harm6);
		
		JLabel lblAnguloDeFase6 = new JLabel("Ângulo de Fase");
		lblAnguloDeFase6.setBounds(113, 87, 132, 15);
		panel_NHarmonicosDados6.add(lblAnguloDeFase6);
		
		JButton button_Harm6 = new JButton("Enviar");
		button_Harm6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					double ampHarmonico6 = Double.parseDouble(Amp_harm6.getText());
					double angHarmonico6 = Double.parseDouble(ang_harm6.getText());
					double ordHarmonico6 = Double.parseDouble(ord_harm6.getText());
					
					dadosUc3.setAmplitudeHarm6(ampHarmonico6);
					dadosUc3.setAnguloHarm6(angHarmonico6);
					dadosUc3.setOrdemHarm6(ordHarmonico6);
					
					graficoHarm6.setScores(dadosUc3.ondasHarmonicas6());
					graficoSaida.setScores(dadosUc3.ondaDistorcida());
					
		
					}
				catch(NumberFormatException err) {
					JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				catch(IllegalArgumentException err) {
					JOptionPane.showMessageDialog(null, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		button_Harm6.setBounds(367, 87, 117, 25);
		panel_NHarmonicosDados6.add(button_Harm6);
		
		JPanel panel_Resultado = new JPanel();
		panel_Resultado.setBounds(0, 792, 1845, 254);
		panelUC3.add(panel_Resultado);
		panel_Resultado.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_GraficoResultado = new JPanel();
		panel_Resultado.add(panel_GraficoResultado);
		panel_GraficoResultado.setLayout(new GridLayout(1, 0, 0, 0));
		graficoSaida=new GraphPanel(new ArrayList<>());
		panel_GraficoResultado.add(graficoSaida);
		graficoSaida.setLayout(null);
		
		JPanel panel = new JPanel();
		panel_Resultado.add(panel);
		panel.setLayout(null);
	}
}
	