package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import View.CasoDeUso2;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CasoDeUso1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CasoDeUso1 frame = new CasoDeUso1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CasoDeUso1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_Menu = new JPanel();
		contentPane.add(panel_Menu, BorderLayout.CENTER);
		panel_Menu.setLayout(null);
		
		JButton btnNewButton = new JButton("Fluxo de Potência Fundamental");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CasoDeUso2 frame = new CasoDeUso2();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			
			}
		});
		btnNewButton.setBounds(58, 51, 383, 53);
		panel_Menu.add(btnNewButton);
		
		JButton btnDistoroHarmnica = new JButton("Distorção Harmônica");
		btnDistoroHarmnica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CasoDeUSo3 frame = new CasoDeUSo3();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			
			
			}
		});
		btnDistoroHarmnica.setBounds(58, 160, 383, 53);
		panel_Menu.add(btnDistoroHarmnica);
		
		
	}
}
